#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jun 21 08:53:36 2021

@author: gabrielqueiroz
"""

# Importing the libraries
import numpy as np
import pandas as pd
import os
import seaborn as sns
import matplotlib.pyplot as plt
from matplotlib.patches import Rectangle

os.chdir('../master_data')
directory = os.getcwd()
file = 'data_2_scenarios_quantity.csv'
path = os.path.join(directory, file)

is_scenario = 'scenario' in file

# Importing the dataset
dataset = pd.read_csv(path)
X = dataset.iloc[:, :-1].values
y = dataset.iloc[:, -1].values

# Transforming features
from sklearn.compose import ColumnTransformer
from sklearn.preprocessing import OneHotEncoder
ct = ColumnTransformer(transformers=[('encoder', OneHotEncoder(), [0])], remainder='passthrough')
X = np.array(ct.fit_transform(X))

# Transforming outcome
from sklearn.preprocessing import LabelEncoder
le = LabelEncoder()
y = le.fit_transform(y)
lable_mapping = {k:v for k,v in enumerate(le.classes_)}

# Splitting the dataset into the Training set and Test set
from sklearn.model_selection import train_test_split
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 0.25, random_state = 1)

# Feature Scaling
from sklearn.preprocessing import StandardScaler
sc = StandardScaler()
if is_scenario:
    X_train = sc.fit_transform(X_train)
    X_test = sc.transform(X_test)
else:
    X_train[:,2:] = sc.fit_transform(X_train[:,2:])
    X_test[:,2:] = sc.transform(X_test[:,2:])


from sklearn.linear_model import LogisticRegression
from sklearn.svm import SVC
from sklearn.ensemble import RandomForestClassifier
from sklearn.multiclass import OneVsOneClassifier
from sklearn.multiclass import OneVsRestClassifier
models = []
models.append(('LR', LogisticRegression(max_iter=1000)))
models.append(('SVM', SVC()))
models.append(('RF', RandomForestClassifier(max_depth=100, random_state=0)))

from sklearn.metrics import make_scorer, accuracy_score, precision_score, recall_score, f1_score
labels = []
cv_results = []
scoring = {'accuracy' : make_scorer(accuracy_score),
           'precision' : make_scorer(precision_score, average='macro', zero_division=0),
           'recall' : make_scorer(recall_score, average='macro', zero_division=0),
           'f1_score' : make_scorer(f1_score, average='macro', zero_division=0)}

# Prepare the cross-validation procedure
for label, model in models:
    from sklearn.model_selection import KFold
    from sklearn.model_selection import cross_validate
    labels.append(label)
    cv = KFold(n_splits=10, random_state=1, shuffle=True)
    scores = cross_validate(model, X_train, y_train, scoring=scoring, cv=cv)
    cv_results.append(scores)
    print('%s Accuracy: %.3f (%.3f)' % (label, np.mean(scores['test_accuracy']), np.std(scores['test_accuracy'])),
          '%s Precision: %.3f (%.3f)' % (label, np.mean(scores['test_precision']), np.std(scores['test_precision'])))

test_accuracy_list = [x['test_accuracy'] for x in cv_results]
test_precision_list = [x['test_precision'] for x in cv_results]

# boxplot algorithm comparison
fig = plt.figure()
fig.suptitle('Accuracy Algorithm Comparison')
ax = fig.add_subplot(111)
plt.boxplot(test_accuracy_list)
ax.set_xticklabels(labels)
plt.savefig('test_accuracy.pdf')
plt.show()

fig = plt.figure()
fig.suptitle('Precision Algorithm Comparison')
ax = fig.add_subplot(111)
plt.boxplot(test_precision_list)
ax.set_xticklabels(labels)
plt.savefig('test_precision.pdf')
plt.show()